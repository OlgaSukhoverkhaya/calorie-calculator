﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Core.Models
{
    public class VipUser
    {
        [Key]
        public int VipUserId { get; set; }    
        public string Gender { get; set; }
        public string BirthDate { get; set; }
        public double Height { get; set; }

        public int UserId { get; set; }
       // public User User { get; set; }
              
        public List<VipProgress> Progress { get; set; }
    }
}
