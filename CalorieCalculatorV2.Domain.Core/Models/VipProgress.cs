﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Core.Models
{
    public class VipProgress
    {
        [Key]
        public int ProgressId { get; set; }
        public Double InitialWeight { get; set; }
        public Double CurrentWeight { get; set; }
        public double CaloriesTarget { get; set; }
        public string Date { get; set; }
             
        public int VipUserId { get; set; }
    //    public VipUser VipUser { get; set; }
    }
}
