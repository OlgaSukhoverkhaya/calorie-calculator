﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Core.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public List<Meal> Meals { get; set; }

        public VipUser VipUser { get; set; }

        public List<UserRole> UserRoles { get; set; }
    }
}
