﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Core.Models
{
    public class Meal
    {
        [Key]
        public int MealId { get; set; }
        public double Weight { get; set; }
        public string Date { get; set; }

        public int UserId { get; set; }
      //  public User User { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

    }
}
