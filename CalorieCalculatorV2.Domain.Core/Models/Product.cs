﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Core.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public double CalorieCount { get; set; }
        public double Fat { get; set; }
        public double Protein { get; set; }
        public double Carbo { get; set; }
              
      //  public List<Meal> Meals { get; set; }


    }
}
