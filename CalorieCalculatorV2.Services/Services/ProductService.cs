﻿using CalorieCalculatorV2.Domain.Core.Models;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Services.Services
{
    public class ProductService:IProductRepository
    {
      private IRepository<Product> productRepository;


        public ProductService(IRepository<Product> repository)
        {
            this.productRepository = repository;
        }

        public Product GetProductByName(string name)
        {
            Product product = productRepository.GetAll().Where(p => p.ProductName == name).FirstOrDefault();
            return product;
        }

        public void Create(Product product)
        {
            productRepository.Create(product);
        }

        public void Update(Product product)
        {
            productRepository.Update(product);
        }

        public void Delete(Product product)
        {
            productRepository.Delete(product);
        }

        public Product GetProductById(int id)
        {
            Product product = productRepository.GetAll().Where(m => m.ProductId == id).FirstOrDefault();
            return product;
        }

        public IQueryable<Product> GetAll()
        {
            return productRepository.GetAll();
        }
    }
}
