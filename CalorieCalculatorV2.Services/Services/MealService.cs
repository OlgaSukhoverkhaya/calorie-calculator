﻿using CalorieCalculatorV2.Domain.Core.Models;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Services.Services
{
    public class MealService : IMealRepository
    {
        private IRepository<Meal> mealRepository;


        public MealService(IRepository<Meal> mealRepository)
        {
            this.mealRepository = mealRepository;
        }

        public async Task CreateAsync(Meal item)
        {
            item.Date = DateTime.Today.ToString("dd.MM.yyyy");
            await this.mealRepository.CreateAsync(item);
        }

        public async Task DeleteAsync(Meal item)
        {
            await this.mealRepository.DeleteAsync(item);
        }

        public async Task<IList<Meal>> GetAllAsync()//TODO product should be included to meals list
        {
            var meals = await this.mealRepository.GetAllAsync();
            return meals.ToList();
            //.Include(x => x.Product);
        }

        public async Task<Meal> GetByIDAsync(int id)//TODO is this async realization good?
        {
            var meals = await mealRepository.GetAllAsync();
            var meal = meals.Where(m => m.MealId == id).FirstOrDefault();
            
            return await Task.FromResult<Meal>(meal);
        }

        public async Task UpdateAsync(Meal item)
        {
           await this.mealRepository.UpdateAsync(item);
        }

        public async Task <IList<Meal>> GetByDateAndUserIdAsync(string date, int userId)//TODO products should be included
        {
            var meals = await this.mealRepository.GetAllAsync();
            var mealTasks = meals.Where(m => m.Date == date && m.UserId == userId).ToList();
            //.Include(x => x.Product).ToList();

                return await Task.FromResult(mealTasks);
        }
    }
}
