﻿using CalorieCalculatorV2.Domain.Core.Models;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace CalorieCalculatorV2.Services.Services
{
   public class UserService : IUserRepository
    {
        private IRepository<User> userRepository;
        private IRepository<Role> roleRepository;
        private IRepository<UserRole> userRoleRepository;
        public UserService(IRepository<User> userRepository, IRepository<Role> roleRepository,IRepository<UserRole> userRoleRepository)
        {
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this.userRoleRepository = userRoleRepository;
        }

        public void Create(User item)
        {
            this.userRepository.Create(item);
        }

        public void Delete(User item)
        {
            this.userRepository.Delete(item);
        }

        public IQueryable<User> GetAll()
        {
            return this.userRepository.GetAll();
        }

        public User GetByUsername(string username)
        {
            User user = this.userRepository.GetAll().Where(u=>u.Username==username).Include(m=>m.Meals).Include(r=>r.UserRoles).FirstOrDefault();
            return user;
        }
       ///**///*///*//*//*/*/*
        public User GetByPasswordAndUserName(string password, string username)
        {
            return this.userRepository.GetAll().FirstOrDefault(x=>x.Password==password && x.Username==username);
        }

        public List<Role> GetRoleByUserId(int userid)
        {   
            return this.roleRepository.GetAll().Where(x => x.UserRoles.Any(u => u.UserId == userid)).ToList();
        }

        public void Update(User item)
        {
            this.userRepository.Update(item);
        }

        public void CreateUserrole(UserRole userrole)
        {
            this.userRoleRepository.Create(userrole);
        }
    }

}
