﻿using CalorieCalculatorV2.Domain.Core.Models;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace CalorieCalculatorV2.Services.Services
{
   public class VipUserService:IVipUserRepository
    {
        private IRepository<VipUser> repository;
        private IRepository<VipProgress> prepository;

        public VipUserService(IRepository<VipUser> repository, IRepository<VipProgress> prepository)
        {
            this.repository = repository;
            this.prepository = prepository;
        }

        public IQueryable<VipUser> GetAll()
        {
            return repository.GetAll();
        }

        public void Create(VipUser vipuser)
        {
            repository.Create(vipuser);
        }

        public void Update(VipUser vipuser)
        {
            repository.Update(vipuser);
        }

        public void Delete(VipUser vipuser)
        {
            repository.Delete(vipuser);
        }

        public VipUser GetByUserId(int id)
        {
            VipUser vuser = repository.GetAll().Where(u => u.UserId == id).Include(p=>p.Progress).FirstOrDefault();
            return vuser;
        }

        public List<VipProgress> GetUserProgresses(int id)
        {
            List<VipProgress> vipProgress = prepository.GetAll().Where(vu => vu.VipUserId == id).ToList();
            return vipProgress;
        }

        public void CreateProgress(VipProgress vp)
        {
            prepository.Create(vp);
        }

        public void DeleteProgress(VipProgress vp)
        {
            prepository.Delete(vp);
        }

    }
}
