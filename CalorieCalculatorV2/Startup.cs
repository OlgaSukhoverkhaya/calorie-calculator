﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using CalorieCalculatorV2.Domain.Interface;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using CalorieCalculatorV2.Services.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace CalorieCalculatorV2
{
    public class Startup
        {
            public Startup(IConfiguration configuration)
            {
                Configuration = configuration;
            }

            public IConfiguration Configuration { get; }

            // This method gets called by the runtime. Use this method to add services to the container.
            public void ConfigureServices(IServiceCollection services)
            {
            services.AddCors();//krossdomen      
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),b=>b.MigrationsAssembly("CalorieCalculatorV2")));
            services.AddMvc();

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IProductRepository,ProductService>();
            services.AddTransient<IMealRepository, MealService>();
            services.AddTransient<IUserRepository, UserService>();
            services.AddTransient<IVipUserRepository, VipUserService>();

            

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                      .AddJwtBearer(options =>
                      {
                          options.RequireHttpsMetadata = false;
                          options.TokenValidationParameters = new TokenValidationParameters
                          {
                              //  укзывает, будет ли валидироваться издатель при валидации токена
                               ValidateIssuer = true,
                               // строка, представляющая издателя
                               ValidIssuer = AuthOptions.ISSUER,

                              ///  будет ли валидироваться потребитель токена
                               ValidateAudience = true,
                              // установка потребителя токена
                              ValidAudience = AuthOptions.AUDIENCE,
                               // будет ли валидироваться время существования
                               ValidateLifetime = true,

                               // установка ключа безопасности
                               IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                             //   валидация ключа безопасности
                               ValidateIssuerSigningKey = true,
                          };
                      })
                  .AddCookie(options => //CookieAuthenticationOptions
                   {
                       options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                   });

        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
            {
                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                //    app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                //    {
                //        HotModuleReplacement = true
                //    });
                }           
                {
                    app.UseExceptionHandler("/api/home");
                }

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());//kross domen
            app.UseAuthentication();

            app.UseStaticFiles();
            app.UseDefaultFiles();
            app.UseMvc();
        }
        }
    }


