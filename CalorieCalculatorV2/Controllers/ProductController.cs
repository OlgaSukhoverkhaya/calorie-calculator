﻿using System.Threading.Tasks;
using CalorieCalculatorV2.Domain.Core.Models;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CalorieCalculatorV2.Controllers
{

    [Produces("application/json")]
    [Route("api/products")]
    public class ProductController : Controller
    {
        IProductRepository repository;

        public ProductController(IProductRepository repository)
        {
            this.repository = repository;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var productList = await repository.GetAllAsync();
            return Ok(productList);
        }

        [Authorize]
        [HttpGet("{name}")]
        public async Task<IActionResult> GetAsync(string name)
        {
            var product = await repository.GetProductByName(name);//TODO rename this method
            return Ok(product);
        }

        [Authorize]
        [Route("~/api/products/id/{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var product = await repository.GetProductById(id);
            return Ok(product);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]Product product)
        {
            if (ModelState.IsValid)
            {
                await repository.CreateAsync(product);
                return Ok(product);
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{name}")]
        public async Task<IActionResult> PutAsync(string name, [FromBody]Product product)
        {
            if (ModelState.IsValid)
            {
                await repository.UpdateAsync(product);
                return Ok(product);
            }
            return BadRequest(ModelState);
        }


        [HttpDelete("{name}")]
        public async Task<IActionResult> DeleteAsync(string name)
        {
            var product = await repository.GetProductByName(name);
            if (product != null)
            {
                await repository.DeleteAsync(product);
            }
            return Ok(product);
        }
    }
}

