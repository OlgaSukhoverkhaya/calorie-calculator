﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalorieCalculatorV2.Domain.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace CalorieCalculatorV2.Controllers
{
    [Route("api/authenticate")]
    public class AccountController : Controller
    {

        IUserRepository repository;

        public AccountController(IUserRepository repository)
        {
            this.repository = repository;
        }

        [HttpPost]
        public async Task Token([FromBody]AuthorizationModel authUser)
        {
          User user = repository.GetByPasswordAndUserName(authUser.Password,authUser.Username);
            var identity = GetIdentity(user);
            if (identity == null)
            {
                Response.StatusCode = 400;
                await Response.WriteAsync("Invalid username or password.");
                return;
            }

            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                user_id=user.UserId
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));

          //  throw new ApplicationException("INVALID_LOGIN_ATTEMPT");
        }

        private ClaimsIdentity GetIdentity(User user)
        {
            List<Role> roles = repository.GetRoleByUserId(user.UserId);
            if (user != null)
            {
                List<Claim> claims = new List<Claim>();
                
                   claims.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, user.Username));

                    foreach(var item in roles)
                {
                    claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, item.Rolename));
                }
                
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            // если пользователя не найдено
            return null;
        }

        [AllowAnonymous]
        [HttpPost("~/api/register/user")]
        public async Task<IActionResult> PostUserAsync([FromBody]User user)
        {
            if (ModelState.IsValid && this.repository.GetByUsernameAsync(user.Username) == null)
            {
                await repository.CreateAsync(user);
                return Ok(true);
            }
            return BadRequest(ModelState);
        }

        [AllowAnonymous]
        [HttpPost("~/api/register/role")]
        public async Task<IActionResult> PostUserRoleAsync([FromBody]UserRole userrole)
        {
            if (ModelState.IsValid)
            {
                await repository.CreateUserRoleAsync(userrole);
                return Ok();
            }
            return BadRequest(ModelState);
        }
    }
}