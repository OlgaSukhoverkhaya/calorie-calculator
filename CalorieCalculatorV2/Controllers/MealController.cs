﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalorieCalculatorV2.Domain.Core.Models;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CalorieCalculatorV2.Controllers
{
    [Produces("application/json")]
    [Route("api/meals")]
    public class MealController : Controller
    {
        IMealRepository repository;

        public MealController(IMealRepository mealRepository)
        {
            this.repository = mealRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var mealsList = await this.repository.GetAllAsync();
            return Ok(mealsList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            Meal meal = await this.repository.GetByIDAsync(id);
            return Ok(meal);
        }

        [Authorize]
        [HttpGet("{userid}/{date}")]
        public async Task<IActionResult> GetByDateAndUseIdAsync(int userid, string date)
        {
            var mealsList = await this.repository.GetByDateAndUserIdAsync(date, userid);
            return Ok(mealsList);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]Meal meal)//TODO should Save be here or not&
        {
            if (ModelState.IsValid)
            {
                await repository.CreateAsync(meal);
                // this.repository.Save();
                return Ok(meal);
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]Meal meal)
        {
            if (ModelState.IsValid)
            {
                await this.repository.UpdateAsync(meal);
                // this.repository.Save();
                return Ok(meal);
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var meal = await this.repository.GetByIDAsync(id);
            if (meal != null)
            {
                await this.repository.DeleteAsync(meal);
                // this.repository.Save();
            }
            return Ok(meal);
        }
    }
}