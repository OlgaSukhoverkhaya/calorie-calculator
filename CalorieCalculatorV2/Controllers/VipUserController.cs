﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalorieCalculatorV2.Domain.Core.Models;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalorieCalculatorV2.Controllers
{
    [Produces("application/json")]
    [Route("api/vipusers")]
    public class VipUserController : Controller
    {
        IVipUserRepository repository;

        public VipUserController(IVipUserRepository repository)
        {
            this.repository = repository;
        }

        [Authorize(Roles = "vipuser")]
        [HttpGet]
        public IEnumerable<VipUser> Get()
        {
            return repository.GetAll();
        }

        [Authorize]
        [Route("~/api/vipusers/userid/{userid}")]
        public VipUser GetByUserId(int userid)
        {
            VipUser vipuser = repository.GetByUserId(userid);
            return vipuser;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody]VipUser vipuser)
        {
            if (ModelState.IsValid)
            {
                repository.Create(vipuser);
                return Ok(vipuser);
            }
            return BadRequest(ModelState);
        }

        [Authorize]
        [HttpPut("~/api/vipusers/update/{vipid}")]
        public IActionResult Put(int vipid,[FromBody]VipUser vipuser)
        {
            if (ModelState.IsValid)
            {
                repository.Update(vipuser);
                return Ok(vipuser);
            }
            return BadRequest(ModelState);
        }

        [Authorize(Roles = "vipuser")]
        [HttpDelete("~/api/vipusers/delete/userid/{userid}")]
        public IActionResult DeleteByUserId(int userid)
        {
            VipUser vipuser = repository.GetByUserId(userid);
            if (vipuser != null)
            {
                repository.Delete(vipuser);
            }
            return Ok(vipuser);
        }

        [Authorize(Roles = "vipuser")]
        [Route("~/api/vipusers/progress/{vuserid}")]
        public List<VipProgress> GetProgressByVipUserId(int vuserid)
        {
            return repository.GetUserProgresses(vuserid);
        }
    }
}
