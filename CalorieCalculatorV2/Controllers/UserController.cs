﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalorieCalculatorV2.Domain.Core.Models;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalorieCalculatorV2.Controllers
{
    [Produces("application/json")]
    [Route("api/users")]
    public class UserController : Controller
    {
        IUserRepository repository;

        public UserController(IUserRepository repository)
        {
            this.repository = repository;
        }

        [Authorize]
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return repository.GetAll();

        }

        [Authorize]
        [HttpGet("~/api/users/userid/{id}")]
        public User GetByUserId(int id)
        {
            return repository.GetAll().Where(i=>i.UserId==id).FirstOrDefault();

        }

        [AllowAnonymous]
        [HttpGet("{username}")]
        public User Get(string username)
        {
            User user = this.repository.GetAll().Where(u=>u.Username==username).FirstOrDefault();
            return user;
        }

        [Authorize]
        [Route("~/api/roles/userid/{id}")]
        public List<string> GetRolenamesByUserId(int id)
        {
            List<string> names = this.repository.GetRoleByUserId(id).Select(e => e.Rolename).ToList();
            return names;
        }
        [Authorize]
        [HttpPut("{username}")]
        public IActionResult Put(string username, [FromBody]User user)
        {
            if (ModelState.IsValid)
            {
                repository.Update(user);
                return Ok(user);
            }
            return BadRequest(ModelState);
        }

        [Authorize]
        [HttpDelete("{username}")]
        public IActionResult Delete(string username)
        {
            User user = this.repository.GetByUsername(username);
            if (user != null)
            {
                repository.Delete(user);
            }
            return Ok(user);
        }
    }
}