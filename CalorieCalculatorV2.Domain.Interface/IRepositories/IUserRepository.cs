﻿using CalorieCalculatorV2.Domain.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Interface.IRepositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByUsernameAsync(string username);
        Task<User> GetByPasswordAndUserNameAsync(string password, string username);
        Task<List<Role>> GetRoleByUserIdAsync(int userid);
        Task CreateUserRoleAsync(UserRole userrole);
    }
}
