﻿using CalorieCalculatorV2.Domain.Core.Models;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Interface.IRepositories
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<Product> GetProductByName(string name);
        Task<Product> GetProductById(int id);
    }
}
