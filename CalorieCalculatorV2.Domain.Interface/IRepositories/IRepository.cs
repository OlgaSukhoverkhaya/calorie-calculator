﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Interface.IRepositories
{
    public interface IRepository<T> where T : class
    {
        Task<IList<T>> GetAllAsync();
        Task CreateAsync(T item);
        Task UpdateAsync(T item);
        Task DeleteAsync(T item);
    }
}
