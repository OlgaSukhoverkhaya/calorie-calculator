﻿using CalorieCalculatorV2.Domain.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Interface.IRepositories
{
    public interface IVipUserRepository : IRepository<VipUser>
    {
        Task<VipUser> GetByUserIdAsync(int id);
        Task<List<VipProgress>> GetUserProgressesAsync(int vuserid);
        Task CreateProgress(VipProgress progress);
        Task DeleteProgress(VipProgress progress);
    }
}
