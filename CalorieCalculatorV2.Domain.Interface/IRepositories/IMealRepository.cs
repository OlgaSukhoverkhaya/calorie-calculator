﻿using CalorieCalculatorV2.Domain.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalorieCalculatorV2.Domain.Interface.IRepositories
{
    public interface IMealRepository : IRepository<Meal>
    {
        Task<IList<Meal>> GetByDateAndUserIdAsync(string date, int userid);
        Task<Meal> GetByIDAsync(int id);
    }
}
