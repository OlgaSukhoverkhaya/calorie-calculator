﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CalorieCalculatorV2.Domain.Interface.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace CalorieCalculatorV2.Domain.Interface
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly ApplicationContext context;
        private DbSet<T> entities;

        public Repository(ApplicationContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }

        public async Task CreateAsync(T item)
        {
            entities.Add(item);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(T item)
        {
            entities.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task<IList<T>> GetAllAsync()
        {
            var entities = await Task.FromResult<DbSet<T>>(this.entities);
            return await entities.ToListAsync();
        }

        public async Task UpdateAsync(T item)
        {
            entities.Update(item);
            await context.SaveChangesAsync();
        }
    }
}
