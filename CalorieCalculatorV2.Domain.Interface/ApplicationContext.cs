﻿using CalorieCalculatorV2.Domain.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace CalorieCalculatorV2.Domain.Interface
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        { }

        public DbSet<Meal> Meals { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<VipUser> VipUsers { get; set; }
        public DbSet<VipProgress> Progresses { get; set; }
    }
}
